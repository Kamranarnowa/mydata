/*
 * Leaflet Heatmap Overlay
 *
 * Copyright (c) 2008-2016, Patrick Wied (https://www.patrick-wied.at)
 * Dual-licensed under the MIT (http://www.opensource.org/licenses/mit-license.php)
 * and the Beerware (http://en.wikipedia.org/wiki/Beerware) license.
 */
(function (name, context, factory) {
  // Supports UMD. AMD, CommonJS/Node.js and browser context
  if (typeof module !== "undefined" && module.exports) {
    module.exports = factory(require("heatmap.js"), require("leaflet"));
  } else if (typeof define === "function" && define.amd) {
    define(["heatmap.js", "leaflet"], factory);
  } else {
    // browser globals
    if (typeof window.h337 === "undefined") {
      throw new Error(
        "heatmap.js must be loaded before the leaflet heatmap plugin"
      );
    }
    if (typeof window.L === "undefined") {
      throw new Error(
        "Leaflet must be loaded before the leaflet heatmap plugin"
      );
    }
    context[name] = factory(window.h337, window.L);
  }
})("HeatmapOverlay", this, function (h337, L) {
  "use strict";

  // Leaflet < 0.8 compatibility
  if (typeof L.Layer === "undefined") {
    L.Layer = L.Class;
  }

  var HeatmapOverlay = L.Layer.extend({
    initialize: function (config) {
      this.cfg = config;
      this._el = L.DomUtil.create("div", "leaflet-zoom-hide");
      this._data = [];
      this._max = 1;
      this._min = 0;
      this.cfg.container = this._el;
    },

    onAdd: function (map) {
      var size = map.getSize();

      this._map = map;

      this._width = size.x;
      this._height = size.y;

      this._el.style.width = size.x + "px";
      this._el.style.height = size.y + "px";
      this._el.style.position = "absolute";

      this._origin = this._map.layerPointToLatLng(new L.Point(0, 0));

      map.getPanes().overlayPane.appendChild(this._el);

      if (!this._heatmap) {
        this._heatmap = h337.create(this.cfg);
      }

      // this resets the origin and redraws whenever
      // the zoom changed or the map has been moved
      map.on("moveend", this._reset, this);
      this._draw();
    },

    addTo: function (map) {
      map.addLayer(this);
      return this;
    },

    onRemove: function (map) {
      // remove layer's DOM elements and listeners
      map.getPanes().overlayPane.removeChild(this._el);

      map.off("moveend", this._reset, this);
    },
    _draw: function () {
      if (!this._map) {
        return;
      }

      var mapPane = this._map.getPanes().mapPane;
      var point = mapPane._leaflet_pos;

      // reposition the layer
      this._el.style[HeatmapOverlay.CSS_TRANSFORM] =
        "translate(" +
        -Math.round(point.x) +
        "px," +
        -Math.round(point.y) +
        "px)";

      this._update();
    },
    _update: function () {
      var bounds, zoom, scale;
      var generatedData = { max: this._max, min: this._min, data: [] };

      bounds = this._map.getBounds();
      zoom = this._map.getZoom();
      scale = Math.pow(2, zoom);

      if (this._data.length == 0) {
        if (this._heatmap) {
          this._heatmap.setData(generatedData);
        }
        return;
      }

      var latLngPoints = [];
      var radiusMultiplier = this.cfg.scaleRadius ? scale : 1;
      var localMax = 0;
      var localMin = 0;
      var valueField = this.cfg.valueField;
      var len = this._data.length;

      while (len--) {
        var entry = this._data[len];
        var value = entry[valueField];
        var latlng = entry.latlng;

        // we don't wanna render points that are not even on the map ;-)
        if (!bounds.contains(latlng)) {
          continue;
        }
        // local max is the maximum within current bounds
        localMax = Math.max(value, localMax);
        localMin = Math.min(value, localMin);

        var point = this._map.latLngToContainerPoint(latlng);
        var latlngPoint = { x: Math.round(point.x), y: Math.round(point.y) };
        latlngPoint[valueField] = value;

        var radius;

        if (entry.radius) {
          radius = entry.radius * radiusMultiplier;
        } else {
          radius = (this.cfg.radius || 2) * radiusMultiplier;
        }
        latlngPoint.radius = radius;
        latLngPoints.push(latlngPoint);
      }
      if (this.cfg.useLocalExtrema) {
        generatedData.max = localMax;
        generatedData.min = localMin;
      }

      generatedData.data = latLngPoints;

      this._heatmap.setData(generatedData);
    },
    setData: function (data) {
      this._max = data.max || this._max;
      this._min = data.min || this._min;
      var latField = this.cfg.latField || "lat";
      var lngField = this.cfg.lngField || "lng";
      var valueField = this.cfg.valueField || "value";

      // transform data to latlngs
      var data = data.data;
      var len = data.length;
      var d = [];

      while (len--) {
        var entry = data[len];
        var latlng = new L.LatLng(entry[latField], entry[lngField]);
        var dataObj = { latlng: latlng };
        dataObj[valueField] = entry[valueField];
        if (entry.radius) {
          dataObj.radius = entry.radius;
        }
        d.push(dataObj);
      }
      this._data = d;

      this._draw();
    },
    // experimential... not ready.
    addData: function (pointOrArray) {
      if (pointOrArray.length > 0) {
        var len = pointOrArray.length;
        while (len--) {
          this.addData(pointOrArray[len]);
        }
      } else {
        var latField = this.cfg.latField || "lat";
        var lngField = this.cfg.lngField || "lng";
        var valueField = this.cfg.valueField || "value";
        var entry = pointOrArray;
        var latlng = new L.LatLng(entry[latField], entry[lngField]);
        var dataObj = { latlng: latlng };

        dataObj[valueField] = entry[valueField];
        this._max = Math.max(this._max, dataObj[valueField]);
        this._min = Math.min(this._min, dataObj[valueField]);

        if (entry.radius) {
          dataObj.radius = entry.radius;
        }
        this._data.push(dataObj);
        this._draw();
      }
    },
    _reset: function () {
      this._origin = this._map.layerPointToLatLng(new L.Point(0, 0));

      var size = this._map.getSize();
      if (this._width !== size.x || this._height !== size.y) {
        this._width = size.x;
        this._height = size.y;

        this._el.style.width = this._width + "px";
        this._el.style.height = this._height + "px";

        this._heatmap._renderer.setDimensions(this._width, this._height);
      }
      this._draw();
    },
  });

  HeatmapOverlay.CSS_TRANSFORM = (function () {
    var div = document.createElement("div");
    var props = [
      "transform",
      "WebkitTransform",
      "MozTransform",
      "OTransform",
      "msTransform",
    ];

    for (var i = 0; i < props.length; i++) {
      var prop = props[i];
      if (div.style[prop] !== undefined) {
        return prop;
      }
    }
    return props[0];
  })();

  return HeatmapOverlay;
});
var clipBorder = {
  type: "FeatureCollection",
  features: [
    {
      type: "Feature",
      id: "USA",
      properties: { name: "United States of America" },
      geometry: {
        type: "MultiPolygon",
        coordinates: [
          [
            [
              [115.81458484707235, -32.058983543853365],
              [115.81447713851838, -32.05900158824145],
              [115.81443893957515, -32.059005833976016],
              [115.81438195426028, -32.05903926911334],
              [115.81424544016659, -32.05911516142294],
              [115.81394799079476, -32.0593274465502],
              [115.81386470395063, -32.059384764651],
              [115.81373758317204, -32.05944579660317],
              [115.81365930714333, -32.059567329411635],
              [115.8135102689032, -32.05950258273883],
              [115.8133136375543, -32.05954450988955],
              [115.81305438573173, -32.059479232157976],
              [115.81289908480665, -32.05953548902256],
              [115.81277259011489, -32.059593336735446],
              [115.81263044035825, -32.05966126784694],
              [115.81249329976185, -32.059811990399645],
              [115.81243380971205, -32.05987461422384],
              [115.81233424246346, -32.060046032984424],
              [115.81225847083414, -32.06017446501132],
              [115.81226911658467, -32.06036711184912],
              [115.81237244074299, -32.060556573611336],
              [115.81263544934518, -32.060658999723586],
              [115.81278699206592, -32.06073489034172],
              [115.81286151121452, -32.06068128929698],
              [115.81293039453135, -32.06063511802084],
              [115.81305814217518, -32.06048545822952],
              [115.8132497628892, -32.060445124496255],
              [115.81358916906976, -32.06041328211096],
              [115.81391667741804, -32.0604196505929],
              [115.8139630170316, -32.0604196505929],
              [115.81428769319962, -32.060265105879516],
              [115.81430408344761, -32.0600440530852],
              [115.814423063476, -32.05969802940228],
              [115.81438792354254, -32.059550899208894],
              [115.81450252057493, -32.05946280081074],
              [115.81463966109271, -32.059305178008564],
              [115.8146290157585, -32.059121020419454],
              [115.81458484707235, -32.058983543853365],
            ],
          ],
        ],
      },
    },
  ],
};
var testData = {
  max: 8,
  data: [
    { lat: -32.0592, lng: 115.8144, count: 10 },
    { lat: -32.0596, lng: 115.8138, count: 10 },
    { lat: -32.0601, lng: 115.814, count: 10 },
    { lat: -32.0603, lng: 115.8126, count: 10 },
    { lat: -32.0606, lng: 115.8127, count: 10 },
  ],
};

var baseLayer = L.tileLayer(
  "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
  {
    attribution:
      'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://cloudmade.com">CloudMade</a>',
    maxZoom: 20,
    noWrap: true,
  }
);

var map = new L.Map("map", {
  layers: [baseLayer],
  center: new L.LatLng(-32.0606, 115.8127),
  zoom: 20,
  preferCanvas: true,
});

var heatmapCfg = {
  radius: 0.0005,
  maxOpacity: 1,
  scaleRadius: true,
  useLocalExtrema: false,
  latField: "lat",
  lngField: "lng",
  valueField: "count",
  gradient: {
    0.0: "#86439b", //purple
    0.3: "#374fa0", //blue
    0.5: "#70bf45", //green
    0.7: "#fcc90b", // yellow
    1.0: "#ef2520", //red
  },
};

var clippedHeatmapLayer = new HeatmapOverlay(heatmapCfg);
clippedHeatmapLayer.addTo(map);

var heatmapLayer = new HeatmapOverlay(heatmapCfg);
heatmapLayer.addTo(map);

heatmapLayer.setData(testData);

var overlayPane = map.getPanes().overlayPane;
var heatmapCanvas = overlayPane.childNodes[1].childNodes[0];
heatmapCanvas.style.display = "none";

var clippedHeatmapCanvas = overlayPane.childNodes[0].childNodes[0];

var clipPane = map.createPane("clip-pane");
clipPane.style.zIndex = 350;
clipPane.style.display = "none";

var clipLayer = L.geoJSON(clipBorder, {
  pane: "clip-pane",
  style: {
    stroke: false,
    fill: true,
    fillColor: "#ffffff",
    fillOpacity: 1,
  },
});
clipLayer.addTo(map);

function showClippedCanvas() {
  var workCanvas = document.createElement("canvas");
  var workCtx = workCanvas.getContext("2d");

  workCanvas.width = heatmapCanvas.width;
  workCanvas.height = heatmapCanvas.height;

  var clipCanvas = clipPane.childNodes[0];
  var clipCanvasW = parseInt(clipCanvas.style.width, 10);
  var clipCanvasH = parseInt(clipCanvas.style.height, 10);
  var xOffset = (clipCanvasW - heatmapCanvas.width) / 2;
  var yOffset = (clipCanvasH - heatmapCanvas.height) / 2;

  workCtx.drawImage(clipCanvas, -xOffset, -yOffset, clipCanvasW, clipCanvasH);
  workCtx.globalCompositeOperation = "source-in";
  workCtx.drawImage(heatmapCanvas, 0, 0);

  var targetCtx = clippedHeatmapCanvas.getContext("2d");
  targetCtx.clearRect(0, 0, heatmapCanvas.width, heatmapCanvas.height);
  targetCtx.drawImage(workCanvas, 0, 0);
}

map.on("moveend", function () {
  setTimeout(showClippedCanvas, 50);
});

setTimeout(showClippedCanvas, 50);
